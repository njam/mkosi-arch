mkosi-arch
==========

Creating an Arch Linux image with [mkosi](https://github.com/systemd/mkosi) for using in a VM.

The post-install script (`./mkosi.postinst`) is installing quite a few packages, for example:
- Pacman, yay
- Gnome
- Firefox
- etc.

Make sure to customize to your need!


Building the Image
------------------

### Setup
Make sure you have "mkosi" and "arch-install-scripts" installed.    

To install these on Arch, using the AUR helper "yay":
```
yay -S mkosi-git arch-install-scripts
```

### Building the Image
```
make build
```
The resulting image will be stored in `./mkosi.output/image.raw`.


Running the Image
-----------------

### Starting a VM with KVM/QEMU
The resulting "raw" image can be booted with KVM/QEMU.

Here's an example to start a VM using [virt-manager](https://virt-manager.org/) / [libvirt](https://libvirt.org/):
```
virt-install \
    --connect qemu:///session \
    --name "my-vm" \
    --memory 5000 \
    --vcpus 4 \
    --import --disk ./mkosi.output/image.raw \
    --os-variant archlinux
```


### Starting a VM with LXC

#### Prerequisites

Setup libvirt (on Arch):                                                                                                                      
```                                                                                                                                         
yay -S virt-manager virt-install virt-viewer ebtables dnsmasq                                                                               
sudo systemctl enable --now libvirtd                                                                                                        
```

Create network "default":
```
sudo virsh net-define ./lxc-config/network.xml
sudo virsh net-autostart default
sudo virsh net-start default
```

#### Creating the container

Create VM:
```
sudo virsh -c lxc:///system create ./lxc-config/domain.xml
```

Connect to text console:
```
sudo virsh -c lxc:///system console test1
```

#### Validate the config

```
virt-xml-validate ./lxc-config/domain.xml
```
