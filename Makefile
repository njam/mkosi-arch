.PHONY: build
build:
	bash -c "trap '$(MAKE) cleanup' EXIT; $(MAKE) build_inner"

.PHONY: build_inner
build_inner:
	sudo mkosi --force build

.PHONY: cleanup
cleanup:
	sudo chown -R "$(shell id -nu):$(shell id -ng)" mkosi.{cache,output}
